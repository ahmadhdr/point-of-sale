import { createRouter, createWebHistory } from 'vue-router'
import MainView from '../pages/main/Main.vue'
import OrderReview from '../pages/order-review/Index.vue'
import OrderComplete from '../pages/order-complete/Index.vue'
import ChooseTable from '../pages/choose-table/Index.vue'
import Customer from '../pages/customer/Index.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: MainView
    },
    {
      path: '/orde/review',
      name: 'order-review',
      component: OrderReview
    },
    {
      path: '/order/table',
      name: 'order-table',
      component: ChooseTable
    },
    {
      path: '/order/customer',
      name: 'order-customer',
      component: Customer
    },
    {
      path: '/order/complete',
      name: 'order-complete',
      component: OrderComplete
    }
  ]
})

export default router
